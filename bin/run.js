'use strict';

const path = require('path');

const pj = require('../package.json');
process.title = `${pj.name}-${pj.version}`;

global.CWD = path.normalize(path.join(__dirname, '..'));

global.relPath = (uri) => {
  return path.normalize(path.join(global.CWD, uri));
};

const redis = require('../src/redis');
const server = require('../src/server');
require('../src/lib/logger');

const start = async () => {
  try {
    console.info('Connecting to redis...');
    await redis.connected();
    console.info('Starting API server...');
    server.start();
  } catch (ex) {
    console.error(ex);
    process.exit(1);
  }
};

start();
