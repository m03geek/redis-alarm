const config = {};
module.exports = config;

config.redis = {
  url: 'redis://127.0.0.1',
  retry: 1000,
  connectTimeout: 60 * 1000,
};

config.server = {
  port: 9000,
};
