#### 1.0.0 (2017-12-16)

##### Build System / Dependencies

* **docker:** add docker file ([d33a44e3](https://gitlab.com/m03geek/redis-alarm/commit/d33a44e3a97c96ad31ad25232d7554b36f65144f))

##### Continuous Integration

* **gitlab:** add CI ([0831a29e](https://gitlab.com/m03geek/redis-alarm/commit/0831a29e6e4acda032e56e10fdefa7e87da72ffa))

##### New Features

* **echo:** add echo server functionality ([2e494420](https://gitlab.com/m03geek/redis-alarm/commit/2e494420fa922b6f7b2946b6c2b292b4560defb8))
* **project:** initial commit ([3d61f924](https://gitlab.com/m03geek/redis-alarm/commit/3d61f92466515f0733a5efd5c3ccc1617fc1bb29))

##### Code Style Changes

* **eslint:** add eslint configs ([e692ed84](https://gitlab.com/m03geek/redis-alarm/commit/e692ed84348583c7893a8d6badea4e4e222373a3))

