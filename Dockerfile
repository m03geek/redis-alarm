FROM mhart/alpine-node:latest
RUN apk add --no-cache dumb-init

ENV NODE_ENV production
ENV SVC_PATH /opt/service

WORKDIR $SVC_PATH
COPY . $SVC_PATH

EXPOSE 9000
#EXPOSE 9229

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["node", "bin/run.js"]
