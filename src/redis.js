const config = require('config').get('redis');
const redis = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisCli = redis.createClient(config.url, {
  retry_strategy: () => {
    return config.retry;
  },
});
const redisSub = redisCli.duplicate();
redisSub.psubscribe('__keyevent@*__:expired');

/**
 * Wait until redis is connected
 * @param {*} cli - redis client
 * @return {Promise} - resolves when redis is connected
 */
const connected = (cli = redisCli) => {
  return new Promise((resolve, reject) => {
    if (cli.connected) {
      resolve();
    } else {
      const to = setTimeout(() => {
        reject();
      }, config.connectTimeout).unref();
      const int = setInterval(() => {
        if (cli.connected) {
          if (to) clearTimeout(to);
          if (int) clearInterval(int);
          resolve();
        }
      }, 50);
    }
  });
};

exports.client = redisCli;
exports.sub = redisSub;
exports.connected = connected;
