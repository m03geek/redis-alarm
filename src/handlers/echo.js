'use strict';

const {client, connected} = require('../redis');

const post = async (ctx) => {
  const {time, message} = ctx.request.body;
  if (!time || !message) {
    ctx.throw(400);
    return;
  }

  const now = new Date().getTime();
  const date = new Date(time).getTime();

  if (date < now || isNaN(date)) {
    ctx.throw(400, 'Wrong date');
    return;
  }

  try {
    await connected();
    const exSec = Math.round((date - now) / 1000);
    // register message in list and return it's index
    // const data = {
    //   date,
    //   message,
    // };
    const id = (await client.rpushAsync('messages', message)) - 1;
    // store message index as expiring value,
    // will handle expiring keys to echo messages
    await client.setexAsync(`msgex:${id}`, exSec, message);
  } catch (ex) {
    ctx.throw(ex);
  }
  ctx.body = 'ok';
};

module.exports = {
  post,
};
