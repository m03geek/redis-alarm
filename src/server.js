'use strict';

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Router = require('koa-oai-router').default;
const config = require('config').get('server');

const start = () => {
  const app = new Koa();
  const server = app.listen(config.port);
  const opt = {
    apiDoc: global.relPath('./src/api/api.yaml'),
    controllerDir: global.relPath('./src/handlers'),
    server,
    versioning: false,
    apiExplorerVisible: true,
  };
  const router = new Router(opt);
  app.use(bodyParser());
  app.use(router.apiExplorerV3());
  app.use(router.routes());
};

exports.start = start;
