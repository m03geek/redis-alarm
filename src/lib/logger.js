'use strict';

const {client, sub} = require('../redis');

let lastMessage = 0;

client.on('connect', async () => {
  // request all messages since last one (will handle non-fatal network issues)
  console.log('Redis connected');
  const messageKeys = await client.keysAsync('msgex:*');
  const start = lastMessage ? lastMessage + 1 : lastMessage;
  let stop;
  if (messageKeys.length) {
    const first = messageKeys
      .map((key) => Number(key.substr(key.indexOf(':') + 1)))
      .sort((a, b) => b - a)[messageKeys.length - 1];
    if (first === 0) {
      return; // no expired messages
    }
    stop = first - 1; // we'll take all messages that have already "expored"
  } else {
    stop = -1;
  }

  try {
    const messages = await client.lrangeAsync('messages', start, stop);
    if (!messages.length) {
      return;
    }
    console.info('Prevoius messages:');
    messages.map(print);
  } catch (ex) {
    console.error('Something went terribly wrong');
  }
});

sub.on('pmessage', async (pattern, _, message) => {
  if (pattern !== '__keyevent@*__:expired') {
    return;
  }
  const messageId = Number(message.substr(message.indexOf(':') + 1));
  if (isNaN(messageId)) {
    return;
  }
  try {
    const message = await client.lindexAsync('messages', messageId);
    print(message);
  } catch (ex) {
    console.error('Failed to find a message', ex);
  }
});

const print = (message) => {
  console.log(`~~~~~ MSG >: ${message}`);
};
