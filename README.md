# redis-alarm

## Requirements

* redis-server
* \>=node-8.0.0

## Usage

### Preparations

In order to make it work we need sligth modifications in redis default configuration, thus it's needed to execute following command:

```sh
redis-cli config set notify-keyspace-events Ex
```

Alsou you may modifiy files in `config` directory in order to set custom params for redis database, server port, etc.

### Starting

Use `npm start` comand to start server with default config

Dockerfile provided in order to run it in docker.

#### PreStart

Ensure that's redis server is running

### Use

Once server is started it will provide simple swagger ui default on http://localhost:9000/api-explorer-v3

Request format could also be found in swagger API via the link above or directly via [api.yaml](src/api/api.yaml)
